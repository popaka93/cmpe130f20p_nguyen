import java.util.*;

class Stack{
    int top=-1;//top pointer
    int stackArray[]=new int[8];
    void push(int x)
    { stackArray[++top]=x; }
    int pop()
    { if(top==-1)
        return 0;
        return stackArray[top--]; }
}

class ArrData{
    String Airline[];
    int flightNumber[];
    long DepartureTime[];
    long ArrivalTime[];
    ArrData(String A[],int flno[],long DT[],long AT[])
    {
        Airline=A;
        flightNumber=flno;
        DepartureTime=DT;
        ArrivalTime=AT;
    }
}

class Time{
    long MinutetoMins(long x)
    {
        return(x%60);
    }
    long MinutetoHrs(long x)
    {
        return(x/60);
    }
    long HourstoMins(long x)
    {
        return(x*60);
    }
}

class VertexNames{
    String VertexNames[]=new String[8];
    VertexNames()
    {
        VertexNames[0]="MDW";//Chicago
        VertexNames[1]="SEA";//Seattle
        VertexNames[2]="PHX";//Phoenix
        VertexNames[3]="LAS";//Las Vegas
        VertexNames[4]="SLC";//Salt Lake City
        VertexNames[5]="SJC";//San Jose
        VertexNames[6]="DFW";//Dallas
        VertexNames[7]="LAX";//Los Angeles
    }
    int getAirportAsIndex(String DepAirport)
    {
        int i=0;
        while(!VertexNames[i].equalsIgnoreCase(DepAirport))//checks if strings are equal
        { i++; }
        return i;
    }
}

public class Flights {
    public static int tot_nodes=8;
    public static int path[]=new int[10];//priority queue containing vertex index
    static Scanner s=new Scanner(System.in);
    static VertexNames AIRPORT=new VertexNames();
    static Time TimeConverter=new Time();
    static ArrData Schedule[]=new ArrData[8];
    static Stack Buffer=new Stack();//A temporary buffer of stack data structure used during displaying information
    static long MinTime;

    //Main function
    public static void main(String[] args){
        int i,j;
        long cost[][]=new long[8][8];//cost adjacency matrix containing intercity flight durations
        long dist[]=new long[8];//shortest path matrix
        String StartTimeString;
        long StartTimeReader[]=new long[2];//Lower address (index 0) will hold hours. Higher address (index 1) will store minutes.
        long startH,startM;
        long startT;
        String DepartureAirport;
        String ArrivalAirport;
        System.out.print("FLIGHT AGENDA\n\n");
        create(cost);
        System.out.print("Enter the departure airport code: ");
        DepartureAirport=s.next();
        i=AIRPORT.getAirportAsIndex(DepartureAirport);
        System.out.print("Enter the departure time (HH:MM): ");
        StartTimeString=s.next();
        StringTokenizer SplitTime=new StringTokenizer(StartTimeString,":");//String tokenizer class has functions to read an input string token by token separated by the defined delimiter ':'
        int k=0;
        while(SplitTime.hasMoreTokens())
        {
            StartTimeReader[k]=Long.parseLong(SplitTime.nextToken());//Converts the read string
            k++;
        }
        startH=StartTimeReader[0];
        startM=StartTimeReader[1];
        MinTime=startT=TimeConverter.HourstoMins(startH)+startM;//Stores the departure time as a minute value.
        System.out.print("Enter the destination airport code: ");
        ArrivalAirport=s.next();
        int A=AIRPORT.getAirportAsIndex(ArrivalAirport);//converts the destination airport code to the corresponding integer
        System.out.println("\nFlights departing from  "+(AIRPORT.VertexNames[i])+" airport at or after "+startH+":"+startM+" to "+(AIRPORT.VertexNames[A])+" are: \n");
        j=A;
        Dijkstra(cost,i,dist);
        if(dist[i]==1441)
            System.out.println("\nNo Path from "+AIRPORT.VertexNames[i]+" to "+AIRPORT.VertexNames[j]);
        else
            display(i,j,dist);//Display the quickest route and schedules
    }

    public static void create(long cost[][])
    {
        int i,j;
        String Airline[];
        int flightNumber[];
        long DepartureTime[];
        long ArrivalTime[];
        for(i=0;i<tot_nodes;i++)
        {
            for(j=0;j<tot_nodes;j++)
            {
                if(i==j)
                    cost[i][j]=0;//The time (cost) of traveling from an airport to the same airport is 0 minutes
                else
                    cost[i][j]=1441;//1440 minutes in 24 hours so say 1441 is infinity
            }
        }
        //We assign a cost of flight duration in the cost adjacency matrix
        cost[0][1]=cost[1][0]=245;//MDW->>SEA 4h05m
        cost[0][6]=cost[6][0]=135;//MDW-->DFW 2h15m
        cost[1][2]=cost[2][1]=180;//SEA-->PHX 3h00m
        cost[1][3]=cost[3][1]=100;//SEA-->LAS 1h40m
        cost[1][5]=cost[5][1]=75;//SEA-->SJC 1h15m
        cost[2][3]=cost[3][2]=60;//PHX-->LAS 1h00m
        cost[2][4]=cost[4][2]=75;//PHX-->SLC 1h15m
        cost[3][5]=cost[5][3]=75;//LAS-->SJC 1h15m
        cost[3][7]=cost[7][3]=70;//LAS-->LAX 1h10m
        cost[4][6]=cost[6][4]=130;//SLC-->DFW 2h10m
        //Database
        //DFW Dallas
        Airline=new String[] {"American","Southwest","American"};
        flightNumber=new int[] {784,486,777,-1};
        DepartureTime=new long[] {630,1050,1080};
        ArrivalTime=new long[] {765,1180,1215};
        Schedule[6]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
        //LAX Los Angeles
        Airline=new String[] {"Southwest","Southwest","Southwest","American"};
        flightNumber=new int[] {433,223,213,197,-1};
        DepartureTime=new long[] {420,720,1020,1320};
        ArrivalTime=new long[] {490,790,1090,1390};
        Schedule[7]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
        //SLC Salt Lake City
        Airline=new String[] {"United Air", "Southwest","American", "Southwest"};
        flightNumber=new int[] {566,311,259,448,-1};
        DepartureTime=new long[] {420,480,660,870};
        ArrivalTime=new long[] {495,610,735,1000};
        Schedule[4]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
        //MDW Chicago
        Airline=new String[] {"American","American","Southwest","Southwest","American"};
        flightNumber=new int[] {648,448,742,445,287,-1};
        DepartureTime=new long[] {120,160,600,700,945};
        ArrivalTime=new long[] {365,405,845,945,1190};
        Schedule[0]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
        //SEA Seattle
        Airline=new String[] {"United Air","American","Southwest","Southwest","American"};
        flightNumber=new int[] {124,667,446,824,334,-1};
        DepartureTime=new long[] {300,360,630,840,1215};
        ArrivalTime=new long[] {425,485,810,1020,1290};
        Schedule[1]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
        //SJC San Jose
        Airline=new String[] {"United Air", "American","United Air","United Air","Southwest"};
        flightNumber=new int[] {156,187,934,438,555,-1};
        DepartureTime=new long[] {480,600,1050,1125,1330};
        ArrivalTime=new long[] {555,675,1125,1200,1405};
        Schedule[5]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
        //PHX Phoenix
        Airline=new String[] {"American","Southwest", "United Air", "American","Southwest","American"};
        flightNumber=new int[] {789,963,846,748,225,499,-1};
        DepartureTime=new long[] {470,480,660,840,1050,1290};
        ArrivalTime=new long[] {590,540,720,900,1125,1365};
        Schedule[2]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
        //LAS Las Vegas
        Airline=new String[] {"Southwest","Southwest","United Air", "American","American","American"};
        flightNumber=new int[] {986,45,965,102,202,333,-1};
        DepartureTime=new long[] {480,510,555,960,1020,1080};
        ArrivalTime=new long[] {580,580,655,1020,1095,1150};
        Schedule[3]=new ArrData(Airline,flightNumber,DepartureTime,ArrivalTime);
    }

    public static void Dijkstra(long[][] cost, int source, long[] dist)
    {
        int i,j,v1,v2;//Vertex pointers
        long minD;//quickest time
        int src[]=new int[10];//src[] to store whether a vertex (airport) has been already visited or not
        for(i=0;i<tot_nodes;i++)
        {
            dist[i]=cost[source][i];
            src[i]=0;
            path[i]=source;//The shortest path priority queue is initially set at source for all i airports
        }
        src[source]=1;//Set source as visited
        for(i=1;i<tot_nodes;i++)
        {
            minD=1441;//initialize minimum flight time to 1441 (infinity)
            v1=-1;
            for(j=0;j<tot_nodes;j++)//Run loop to compare all destinations and choose the unvisted airport with minimum flight duration
            {
                if(src[j]==0)//If Airport unvisited
                {
                    if(dist[j]<minD)//if duration to the jth airport from source is less than current minimum, select it
                    {
                        minD=dist[j];
                        v1=j;
                    }
                }
            }
            src[v1]=1;
            for(v2=0;v2<tot_nodes;v2++)//Update the duration values of all adjacent airports
            {
                if(src[v2]==0)//if v2 is unvisited
                {
                    if((dist[v1]+cost[v1][v2])<dist[v2])//if total flight time of v2<--v1<--source is less than the direct flight duration of v2<--source then relax this edge
                    {
                        dist[v2]=dist[v1]+cost[v1][v2];//Route is from source to v1 to v2
                        path[v2]=v1;//path is via v1
                    }
                }
            }
        }
    }

    public static void display(int Source,int Destination,long dist[])
    {
        int i;
        System.out.println("The route from "+AIRPORT.VertexNames[Source]+" to "+AIRPORT.VertexNames[Destination]+" is: \n");
        for(i=Destination;i!=Source;i=path[i])
        {
            System.out.print(AIRPORT.VertexNames[i]+" <-- ");
            Buffer.push(i);//Path index is stored in a stack for later use (display of Arrival/Departure data)
        }
        System.out.println(" "+AIRPORT.VertexNames[i]);
        Buffer.push(i);//Push the source
        System.out.println("\nThe Flight Details on your route are: \n");
        showData(Destination);
        System.out.println("\nThe total flight time (excluding halts) is: "+TimeConverter.MinutetoHrs(dist[Destination])+" hours "+TimeConverter.MinutetoMins(dist[Destination])+" minutes");//Displays total flight time. The Minute value is converted to a 24h time.
    }

    public static void showData(int dest)
    {
        int i=Buffer.pop();//pops the top element(departure airport)
        Stack ArrivalStack=new Stack();
        while(i!=dest)//while the airport index (popped from buffer) is not equal to the destination airport we display data
        {
            System.out.println("From Airport "+AIRPORT.VertexNames[i]+"\n\nFLIGHT\t\t\tDESTINATION\tDEPARTURE\tARRIVAL\n_______________________________________________________________________");
            for(int j=0;Schedule[i].flightNumber[j]!=-1;j++)
            {
                int k=Buffer.pop();//pop the next airport (destination of current leg) to obtain the arrival time at that airport.
                Buffer.push(k);
                if(Schedule[i].DepartureTime[j]<MinTime)//If the departure time of the flight is already elapsed, (MinTime stores departure time), then that flight should not be diplayed. The loop is run to the next iteration.
                    continue;
                ArrivalStack.push(j);//Push the current flight index into the stack.
                System.out.println(Schedule[i].Airline[j]+" "+Schedule[i].flightNumber[j]+"\t    "+AIRPORT.VertexNames[k]+"\t\t"+TimeConverter.MinutetoHrs(Schedule[i].DepartureTime[j])+":"+TimeConverter.MinutetoMins(Schedule[i].DepartureTime[j])+"\t\t   "+TimeConverter.MinutetoHrs(Schedule[i].ArrivalTime[j])+":"+TimeConverter.MinutetoMins(Schedule[i].ArrivalTime[j]));
            }
            System.out.println();
            int LIMIT=0;
            while(ArrivalStack.top!=-1)//Until we do not pop all elements from stack do:
            {
                LIMIT=ArrivalStack.pop();
            }
            MinTime=Schedule[i].ArrivalTime[LIMIT];//Set the MinTime as the arrival time of the earliest flight reaching the next leg of the journey.
            i=Buffer.pop();//update the next airport
        }
        System.out.println();
        Buffer.pop();//Empties the buffer
    }
}